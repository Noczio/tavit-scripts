import unittest
from core.parser.path import formatKnownSites


class TestFormatKnownSites(unittest.TestCase):
    
    def test_single_site(self):
        sitesList = ['path/to/site.vcf']
        expected_command = '--known-sites path/to/site.vcf'
        self.assertEqual(formatKnownSites(sitesList), expected_command)

    def test_multiple_sites(self):
        sitesList = ['path/to/site1.vcf', 'path/to/site2.vcf']
        expected_command = '--known-sites path/to/site1.vcf --known-sites path/to/site2.vcf'
        self.assertEqual(formatKnownSites(sitesList), expected_command)

    def test_empty_sites_list(self):
        sitesList = []
        with self.assertRaises(Exception):
            formatKnownSites(sitesList)


if __name__ == '__main__':
    unittest.main()
