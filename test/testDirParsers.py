import unittest
import os
from core.parser.path import knownSitesDirParser, fastqDirParser, fastaDirParser, absolutePath


class TestKnownSitesDirParser(unittest.TestCase):

    def test_num_files(self):
        sitesDir = absolutePath('test/data/sites/complete')
        resultParser = knownSitesDirParser(sitesDir)
        sitesLen = len(resultParser['sites'])
        indexesLen = len(resultParser['indexes'])

        self.assertEqual(sitesLen, 2)
        self.assertEqual(indexesLen, 2)
        self.assertEqual(sitesLen, indexesLen)

    def test_no_files(self):
        sitesDir = absolutePath('test/data/sequences')
        with self.assertRaises(Exception):
            knownSitesDirParser(sitesDir)

    def test_num_vcf_files_mismatch(self):
        sitesDir = absolutePath('test/data/sites/missing')
        with self.assertRaises(Exception):
            knownSitesDirParser(sitesDir)


class TestFastqDirParser(unittest.TestCase):

    def test_pe_files(self):
        fastqDir = absolutePath('test/data/sequences/pe')
        result = fastqDirParser(fastqDir)

        self.assertTrue(os.path.exists(result['file1']))
        self.assertTrue(os.path.exists(result['file2']))
        self.assertEqual(result['trimmingMode'], 'PE')


    def test_se_files(self):
        fastqDir = absolutePath('test/data/sequences/se')
        fastqs = fastqDirParser(fastqDir)

        self.assertTrue(os.path.exists(fastqs['file1']))
        self.assertEqual(fastqs['file2'], '')
        self.assertEqual(fastqs['trimmingMode'], 'SE')

    def test_no_files(self):
        fastqDir = absolutePath('test/data/genome/complete')
        with self.assertRaises(Exception):
            fastqDirParser(fastqDir)

    def test_multiple_files(self):
        fastqDir = absolutePath('test/data/sequences/more than necessary')
        with self.assertRaises(Exception):
            fastqDirParser(fastqDir)


class TestFastaDirParser(unittest.TestCase):
    
    def test_one_file(self):
        fastaDir = absolutePath('test/data/genome/complete')
        result = fastaDirParser(fastaDir)
        self.assertTrue(os.path.exists(result))

    def test_no_files(self):
        fastaDir = absolutePath('test/data/sites/complete')
        with self.assertRaises(Exception):
            fastaDirParser(fastaDir)

    def test_multiple_files(self):
        fastaDir = absolutePath('test/data/genome/more than necessary')
        result = fastaDirParser(fastaDir)
        self.assertTrue(os.path.exists(result))


if __name__ == '__main__':
    unittest.main()
