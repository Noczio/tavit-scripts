import os
import unittest
from core.tools.gatk import alignmentSummaryMetrics, dictGenome, insertSizeMetrics, markDuplicatesSpark
from core.tools.bwa import alignmentPE, indexBWA, createRgLine
from core.tools.samtools import depthBam, indexGenome
from core.parser.path import loadJson, absolutePath, fastqDirParser, fastaDirParser, extractFileName


class TestProcessIncomplete(unittest.TestCase):

    def setUp(self):
        self.defaultParameters = loadJson(absolutePath('defaults/parameters.json'))
        self.defaultFilesPath = loadJson(absolutePath('defaults/files.json'))
        self.trimmomaticPaths = self.defaultFilesPath['trimmomatic']
        self.bwaPaths = self.defaultFilesPath['bwa']
        self.samtoolsPath = self.defaultFilesPath['samtools']
        self.gatkPath = self.defaultFilesPath['gatk']
        self.adapterPaths = self.defaultFilesPath['adapter']
        self.out = absolutePath('test/data/real')

        self.fastqPaths = fastqDirParser(self.out)
        self.referenceGenome = fastaDirParser(self.out)

        self.file1 = self.fastqPaths['file1']
        self.file2 = self.fastqPaths['file2']
        self.trimmingMode = self.fastqPaths['trimmingMode']
    
    def tearDown(self):
        filesToKeep = [self.file1, self.file2, self.referenceGenome]
        for filename in os.listdir(self.out):
            filePath = os.path.join(self.out, filename)
            if filePath not in filesToKeep:
                os.remove(filePath)

    def test_alignmentPE(self):
        threads = self.defaultParameters['shared']['threads']['default']
        
        sm = extractFileName(self.file1)
        pl = self.defaultParameters['bwa']['pl']['default'] 
        pm = self.defaultParameters['bwa']['pm']['default'] 
        lb = self.defaultParameters['bwa']['lb']['default'] 
        sid = f"{sm}_{lb}"
        rgLine = createRgLine(sid, lb, pl, pm, sm)

        _ = indexBWA(self.referenceGenome)
        _ = indexGenome(self.referenceGenome)
        _ = dictGenome(self.referenceGenome)
        
        alignmentResult = alignmentPE(self.referenceGenome, rgLine,
                                      threads, self.file1, self.file2, self.bwaPaths, self.out)
        
        sam = alignmentResult['sam']

        markDuplicatesResult = markDuplicatesSpark(sam, self.gatkPath, self.out)
        markedBam = markDuplicatesResult['marked']

        self.assertTrue(os.path.exists(markDuplicatesResult['marked']))

        alignmentSummaryMetricsResult = alignmentSummaryMetrics(markedBam, self.referenceGenome, self.gatkPath, self.out)
        insertSizeMetricsResult = insertSizeMetrics(markedBam, self.referenceGenome, self.gatkPath, self.out)
        depthBamResult = depthBam(markedBam, self.samtoolsPath, self.out)

        self.assertTrue(os.path.exists(alignmentSummaryMetricsResult['summary']))
        self.assertTrue(os.path.exists(insertSizeMetricsResult['metrics']))
        self.assertTrue(os.path.exists(insertSizeMetricsResult['histogram']))
        self.assertTrue(os.path.exists(depthBamResult['depth']))
        
        

if __name__ == '__main__':
    unittest.main()
