import os
import unittest
from core.tools.trimmomatic import trimmingPE
from core.parser.path import loadJson, absolutePath, fastqDirParser, fastaDirParser, extractFileName


class TestTrimming(unittest.TestCase):

    def setUp(self):
        self.defaultParameters = loadJson(absolutePath('defaults/parameters.json'))
        self.defaultFilesPath = loadJson(absolutePath('defaults/files.json'))
        self.trimmomaticPaths = self.defaultFilesPath['trimmomatic']
        self.adapterPaths = self.defaultFilesPath['adapter']
        self.out = absolutePath('test/data/real')

        self.fastqPaths = fastqDirParser(self.out)
        self.referenceGenome = fastaDirParser(self.out)

        self.file1 = self.fastqPaths['file1']
        self.file2 = self.fastqPaths['file2']
        self.trimmingMode = self.fastqPaths['trimmingMode']
    
    def tearDown(self):
        filesToKeep = [self.file1, self.file2, self.referenceGenome]
        for filename in os.listdir(self.out):
            filePath = os.path.join(self.out, filename)
            if filePath not in filesToKeep:
                os.remove(filePath)

    def test_trimmingPE(self):
        adapterChosen = self.defaultParameters['trimmomatic']['adapter']['default']
        adapter = absolutePath(self.adapterPaths[self.trimmingMode][adapterChosen])
        encoding = self.defaultParameters['trimmomatic']['encoding']['default']
        leading = self.defaultParameters['trimmomatic']['leading']['default']
        trailing = self.defaultParameters['trimmomatic']['trailing']['default']
        slidingWindowSize = self.defaultParameters['trimmomatic']['slidingWindowSize']['default']
        slidingWindowMinQuality = self.defaultParameters['trimmomatic']['slidingWindowMinQuality']['default']
        seedMismatches = self.defaultParameters['trimmomatic']['seedMismatches']['default']
        palindromeClipThreshold = self.defaultParameters['trimmomatic']['palindromeClipThreshold']['default']
        simpleClipThreshold = self.defaultParameters['trimmomatic']['simpleClipThreshold']['default']
        minlen = self.defaultParameters['trimmomatic']['minlen']['default']

        threads = self.defaultParameters['shared']['threads']['default']

        trimmingResult = trimmingPE(threads, encoding, self.file1, self.file2, adapter, leading, trailing, slidingWindowSize, slidingWindowMinQuality,
                                    seedMismatches, palindromeClipThreshold, simpleClipThreshold, minlen, self.trimmomaticPaths, self.out)

        self.assertTrue(os.path.exists(trimmingResult['trimmed1']))
        self.assertTrue(os.path.exists(trimmingResult['trimmed2']))
        self.assertTrue(os.path.exists(trimmingResult['trimmedUnpaired1']))
        self.assertTrue(os.path.exists(trimmingResult['trimmedUnpaired2']))
        

if __name__ == '__main__':
    unittest.main()
