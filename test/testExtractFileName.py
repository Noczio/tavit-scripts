import unittest
from core.parser.path import extractFileName

class TestExtractFileName(unittest.TestCase):

    def test_extract_file_name(self):
        
        self.assertEqual(extractFileName("SRR005968_1.fastq"), "SRR005968")
        self.assertEqual(extractFileName("path/to/file/SRR18802909.fastq.gz"), "SRR18802909")
        self.assertEqual(extractFileName("filename.txt"), "filename")
        self.assertEqual(extractFileName("/path/to/another/filename"), "filename")
        self.assertEqual(extractFileName("file.with.dots.txt"), "file")
        self.assertEqual(extractFileName("file_with_no_extension"), "file")
        self.assertEqual(extractFileName("SRR123456_"), "SRR123456")
        self.assertEqual(extractFileName("SRR789_abc.txt"), "SRR789")
        self.assertEqual(extractFileName("SRR999_123.fastq"), "SRR999")
        self.assertEqual(extractFileName("SRR999_456_789.txt"), "SRR999")
        self.assertEqual(extractFileName("SRR1234_5678.txt.gz"), "SRR1234")

if __name__ == '__main__':
    unittest.main()
