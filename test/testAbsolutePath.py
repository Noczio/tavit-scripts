import unittest
import os
from core.parser.path import absolutePath


class TestAbsolutePath(unittest.TestCase):
    
    def test_absolutePath_exists(self):
        absPath = absolutePath('test/data/defaults/files.json')
        self.assertTrue(os.path.exists(absPath))

    def test_absolutePath_not_exists(self):
        absPath = absolutePath('random/path')
        self.assertFalse(os.path.exists(absPath))


if __name__ == '__main__':
    unittest.main()
