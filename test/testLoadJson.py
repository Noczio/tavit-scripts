import unittest
from core.parser.path import loadJson, absolutePath


class TestJson(unittest.TestCase):
    
    def test_valid_json(self):
        filepath = absolutePath('test/data/defaults/files.json')
        jsonData = loadJson(filepath)
        expected = {'SE': {
            'trueSeq2': 'adapters/TruSeq2-SE.fa',
            'trueSeq3': 'adapters/TruSeq3-SE.fa'
        },
            'PE': {
            'trueSeq2': 'adapters/TruSeq2-PE.fa',
            'trueSeq3': 'adapters/TruSeq3-PE.fa',
            'trueSeq3 version 2': 'adapters/TruSeq3-PE-2.fa',
            'nextera': 'adapters/NexteraPE-PE.fa'
        }}

        self.assertEqual(jsonData['adapter'], expected)

    def test_invalid_json(self):
        filepath = 'path/to/invalid_json.json'
        with self.assertRaises(Exception):
            loadJson(filepath)


if __name__ == '__main__':
    unittest.main()
