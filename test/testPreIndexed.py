import unittest

from core.parser.path import absolutePath
from core.validator.exists import extensionsExist


class TestExtensionsExist(unittest.TestCase):
    def test_extensionsExist_with_valid_file_path(self):
        file_path = absolutePath('test/data/index/file')
        extensions = [".txt", ".csv"]
        result = extensionsExist(file_path, extensions)
        self.assertTrue(result)

    def test_extensionsExist_with_missing_file_path(self):
        file_path = '/path/to/missing_file'
        extensions = [".txt", ".csv"]
        result = extensionsExist(file_path, extensions)
        self.assertFalse(result)

    def test_extensionsExist_with_empty_extensions_list(self):
        file_path = '/path/to/file'
        extensions = []
        with self.assertRaises(Exception):
            extensionsExist(file_path, extensions)

    def test_extensionsExist_with_changesOriginal_true(self):
        file_path = '/path/to/file.txt'
        extensions = ['.tsv']
        result = extensionsExist(file_path, extensions, True)
        self.assertFalse(result)


if __name__ == '__main__':
    unittest.main()
