import unittest
import sys

from core.parser.path import absolutePath
from core.request.question import askForPath


class TestInitialAppArguments(unittest.TestCase):

    def setUp(self):
        self.realPath = absolutePath('test/data/real')
        self.notExistantPath = '/random/path'
    
    def tearDown(self):
        self.realPath = ''
        self.notExistantPath = ''

    def test_valid_path(self):
        sys.argv = ['app.py', self.realPath]
        expected_result = {'path':  self.realPath, 'useTrimming': False}
        self.assertEqual(askForPath('Path does not exist', 'Argument not provided', 'Incorrect argument'), expected_result)

    def test_valid_path_with_trim_option(self):
        sys.argv = ['app.py', self.realPath, '--t']
        expected_result = {'path': self.realPath, 'useTrimming': True}
        self.assertEqual(askForPath('Path does not exist', 'Argument not provided', 'Incorrect argument'), expected_result)

    def test_valid_path_with_short_trim_option(self):
        sys.argv = ['app.py', self.realPath, '-trim']
        expected_result = {'path': self.realPath, 'useTrimming': True}
        self.assertEqual(askForPath('Path does not exist', 'Argument not provided', 'Incorrect argument'), expected_result)

    def test_path_not_exist(self):
        sys.argv = ['app.py', self.notExistantPath]
        with self.assertRaises(Exception):
            askForPath('Path does not exist', 'Argument not provided', 'Incorrect argument')

    def test_path_not_provided(self):
        sys.argv = ['app.py']
        with self.assertRaises(Exception):
            askForPath('Path does not exist', 'Argument not provided', 'Incorrect argument')
    
    def test_iconrrect_argument(self):
        sys.argv = ['app.py', self.realPath, '-RANDOM']
        with self.assertRaises(Exception):
            askForPath('Path does not exist', 'Argument not provided', 'Incorrect argument')

if __name__ == '__main__':
    unittest.main()