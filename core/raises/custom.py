class CustomException(Exception):
    def __init__(self, *args):
        message = ' '.join(args)  # type: ignore
        super().__init__(message)


class QuantityMismatch(CustomException):
    def __init__(self, *args):
        super().__init__(*args)


class DataNotFound(CustomException):
    def __init__(self, *args):
        super().__init__(*args)


class PathDoesNotExist(CustomException):
    def __init__(self, *args):
        super().__init__(*args)


class ArgumentNotProvided(CustomException):
    def __init__(self, *args):
        super().__init__(*args)


class IncorrectTrimmingArgument(CustomException):
    def __init__(self, *args):          
        super().__init__(*args)
