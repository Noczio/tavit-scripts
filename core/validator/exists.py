import os

from core.raises.custom import DataNotFound
from core.validator.types import vector


def extensionsExist(filePath: str, extensions: vector, changesOriginal: bool = False) -> bool:
    if len(extensions) == 0:
        raise DataNotFound('Extensions list does not have elements.')
    
    for ext in extensions:
        if changesOriginal:
            baseName = os.path.splitext(os.path.basename(filePath))[0]
            folderName = os.path.dirname(filePath)
            pathToTest = os.path.join(folderName, baseName + ext)
        else:
            pathToTest = filePath + ext

        if not os.path.exists(pathToTest):
            return False
        
    return True
