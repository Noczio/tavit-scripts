from typing import Union


anyNumber = Union[int, float, None]
numeric = Union[int, float]
vector = Union[list, tuple]
