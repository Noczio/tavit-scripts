import os
import sys

from core.raises.custom import IncorrectTrimmingArgument, PathDoesNotExist, ArgumentNotProvided


def askForPath(PathNotExistMessage: str, argNotProvidedMessage: str, incorrectArgument: str) -> dict:
    argsLen = len(sys.argv)

    if argsLen < 2:
        raise ArgumentNotProvided(argNotProvidedMessage)

    pathSelected = sys.argv[1]
    
    if argsLen == 2:
        trimSequence = False
    elif argsLen > 2 and sys.argv[2].lower() in ('-trim', '--t'):
        trimSequence = True
    else:
        raise IncorrectTrimmingArgument(incorrectArgument)

    if os.path.exists(pathSelected):
        return {'path': pathSelected, 'useTrimming': trimSequence}
    else:
        raise PathDoesNotExist(PathNotExistMessage)
