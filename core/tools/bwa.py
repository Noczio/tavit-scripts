import os

from core.main.pretify import separatorBefore


def generatePU(pl: str, pm: str, sm: str, rgid: str, lane: str) -> str:
    return f"{pl}.{pm}.{sm}.{rgid}.{lane}"


def createRgLine(sid: str, lb: str, pl: str, pm: str, sm: str) -> str:
    return "@RG\\tID:{0}\\tLB:{1}\\tPL:{2}\\tPM:{3}\\tSM:{4}".format(sid, lb, pl, pm, sm)


@separatorBefore
def indexBWA(referenceGenome: str) -> dict[str, str]:
    argument = "bwa index {0}".format(referenceGenome)
    os.system(argument)

    return {'command': argument}


@separatorBefore
def alignmentSE(referenceGenome: str, rgLine: str, threads: int, trimmedSE: str, defaultFilesPath: dict[str, str], out: str) -> dict[str, str]:
    outputSam = os.path.join(out, defaultFilesPath['sam'])
    
    argument = "bwa mem -t {0} -Y -R '{1}' {2} {3} > {4}".format(threads, rgLine, referenceGenome, trimmedSE, outputSam)
    os.system(argument)

    return {'command': argument, 'sam': outputSam}


@separatorBefore
def alignmentPE(referenceGenome: str, rgLine: str, threads: int, trimmedPE1: str, trimmedPE2: str, defaultFilesPath: dict[str, str], out: str) -> dict[str, str]:
    outputSam = os.path.join(out, defaultFilesPath['sam'])

    argument = "bwa mem -t {0} -Y -R '{1}' {2} {3} {4} > {5}".format(threads, rgLine, referenceGenome, trimmedPE1, trimmedPE2, outputSam)
    os.system(argument)

    return {'command': argument, 'sam': outputSam}
