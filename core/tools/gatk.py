import os

from core.main.pretify import separatorBefore


@separatorBefore
def alignmentSummaryMetrics (bam: str, referenceGenome: str, defaultFilesPath: dict[str, str], out: str) -> dict[str, str]:
    alignmentSummaryMetricsPath = os.path.join(out, defaultFilesPath['alignmentSummaryMetrics'])
    alignmentChartPath = os.path.join(out, defaultFilesPath['alignmentChart'])

    argument = 'gatk CollectAlignmentSummaryMetrics -I {0} -R {1} -O {2} -H {3}'.format(bam, referenceGenome, alignmentSummaryMetricsPath, alignmentChartPath)
    os.system(argument)

    return {'command': argument, 'summary': alignmentSummaryMetricsPath}


@separatorBefore
def insertSizeMetrics (bam: str, referenceGenome: str, defaultFilesPath: dict[str, str], out: str) -> dict[str, str]:
    insertSizeMetricsPath = os.path.join(out, defaultFilesPath['insertSizeMetrics'])
    insertSizeHistogramPath = os.path.join(out, defaultFilesPath['insertSizeHistogram'])

    argument = 'gatk CollectInsertSizeMetrics -I {0} -R {1} -O {2} -H {3}'.format(bam, referenceGenome,  insertSizeMetricsPath, insertSizeHistogramPath)
    os.system(argument)

    return {'command': argument, 'metrics': insertSizeMetricsPath, 'histogram': insertSizeHistogramPath}


@separatorBefore
def selectVariants(rawVariants: str, referenceGenome: str, defaultFilesPath: dict[str, str], out: str) -> dict[str, str]:
    indelsPath = os.path.join(out, defaultFilesPath['indels'])
    snpsPath = os.path.join(out, defaultFilesPath['snps'])

    argumentSnp = 'gatk SelectVariants -R {0} -V {1} --select-type-to-include SNP -O {2}'.format(referenceGenome, rawVariants, snpsPath)

    os.system(argumentSnp)

    argumentIndel = 'gatk SelectVariants -R {0} -V {1} --select-type-to-include INDEL -O {2}'.format(referenceGenome, rawVariants, indelsPath)

    os.system(argumentIndel)

    finalArgument = f"{argumentSnp} && {argumentIndel}"

    return {'command': finalArgument, 'indels': indelsPath, 'snps': snpsPath}


@separatorBefore
def markDuplicatesSpark(sam: str, defaultFilesPath: dict[str, str], out: str) -> dict[str, str]:
    markedPath = os.path.join(out, defaultFilesPath['duplicates'])
    markedTxtPath = os.path.join(out, defaultFilesPath['dump'])

    argument = 'gatk MarkDuplicatesSpark -I {0} -O {1} -M {2}'.format(
        sam, markedPath, markedTxtPath)
    os.system(argument)

    return {'command': argument, 'marked': markedPath, 'dump': markedTxtPath}


@separatorBefore
def baseRecalibrator(markedBam: str, referenceGenome: str, knownSites: str, defaultFilesPath: dict[str, str], out: str) -> dict[str, str]:
    recalTablePath = os.path.join(out, defaultFilesPath['table'])

    argument = 'gatk BaseRecalibrator -I {0} -R {1} {2} -O {3}'.format(
        markedBam, referenceGenome, knownSites, recalTablePath)
    os.system(argument)

    return {'command': argument, 'recalTable': recalTablePath}


@separatorBefore
def applyBQSR(markedBam: str, bsqrTable: str, defaultFilesPath: dict[str, str], out: str) -> dict[str, str]:
    recalibratedBamPath = os.path.join(out, defaultFilesPath['recalibrated'])

    argument = 'gatk ApplyBQSR -I {0} -bqsr {1} -O {2}'.format(markedBam, bsqrTable, recalibratedBamPath)
    os.system(argument)

    return {'command': argument, 'recalibrated': recalibratedBamPath}


@separatorBefore
def dictGenome(referenceGenome: str) -> dict[str, str]:
    baseName = os.path.splitext(os.path.basename(referenceGenome))[0]
    folderName = os.path.dirname(referenceGenome)
    dictFilePath = os.path.join(folderName, baseName + '.dict')

    argument = 'gatk CreateSequenceDictionary -R {0} -O {1}'.format(
        referenceGenome, dictFilePath)
    os.system(argument)

    return {'command': argument, 'dict': dictFilePath}


@separatorBefore
def variantIdentification(referenceGenome: str, recalibratedBam: str, defaultFilesPath: dict[str, str], out: str) -> dict[str, str]:
    variantsPath = os.path.join(out, defaultFilesPath['variants'])

    argument = 'gatk HaplotypeCaller -R {0} -I {1} -O {2}'.format(
        referenceGenome, recalibratedBam, variantsPath)
    os.system(argument)

    return {'command': argument, 'variants': variantsPath}
