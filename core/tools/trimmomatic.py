import os

from core.main.pretify import separatorBefore


@separatorBefore
def trimmingSE(threads: int, encoding: str, fileSE: str, adapter: str, leading: int, trailing: int, slideWindowSize: int, slideWindowMinQuality: int, seedMismatches: int, palindromeClipThreshold: int, simpleClipThreshold: int, minlen: int, defaultFilesPath: dict[str, str], out: str) -> dict[str, str]:
    outputPath1 = os.path.join(out, defaultFilesPath['trimmed1'])

    argument = "trimmomatic SE -threads {0} -{1} {2} {3} ILLUMINACLIP:{4}:{5}:{6}:{7} LEADING:{8} TRAILING:{9} SLIDINGWINDOW:{10}:{11} MINLEN:{12}".format(threads, encoding, fileSE, outputPath1, adapter, seedMismatches, palindromeClipThreshold, simpleClipThreshold, leading, trailing, slideWindowSize, slideWindowMinQuality, minlen)
    os.system(argument)

    return {'command': argument, 'trimmed1': outputPath1}


@separatorBefore
def trimmingPE(threads:int, encoding:str, filePE1: str, filePE2: str, adapter: str, leading: int, trailing: int, slideWindowSize: int, slideWindowMinQuality: int, seedMismatches: int, palindromeClipThreshold: int, simpleClipThreshold: int, minlen: int, defaultFilesPath: dict[str, str], out: str) -> dict[str, str]:
    outputPath1 = os.path.join(out, defaultFilesPath['trimmed1'])
    outputPath2 = os.path.join(out, defaultFilesPath['trimmed2'])
    outputPathUnpaired1 = os.path.join(out, defaultFilesPath['unapaired1'])
    outputPathUnpaired2 = os.path.join(out, defaultFilesPath['unapaired2'])

    argument = "trimmomatic PE -threads {0} -{1} {2} {3} {4} {5} {6} {7} ILLUMINACLIP:{8}:{9}:{10}:{11} LEADING:{12} TRAILING:{13} SLIDINGWINDOW:{14}:{15} MINLEN:{16}".format(threads, encoding, filePE1, filePE2, outputPath1, outputPathUnpaired1, outputPath2, outputPathUnpaired2, adapter, seedMismatches, palindromeClipThreshold, simpleClipThreshold, leading, trailing, slideWindowSize, slideWindowMinQuality, minlen)
    os.system(argument)

    return {'command': argument, 'trimmed1': outputPath1, 'trimmed2': outputPath2, 'trimmedUnpaired1': outputPathUnpaired1, 'trimmedUnpaired2': outputPathUnpaired2}
