import os


def indexAlignment(bam: str, threads: int) -> dict[str, str]:
    indexFile = ''.join([bam, ".bai"])

    argument = "samtools index -@ {0} {1}".format(threads, bam)
    os.system(argument)

    return {'command': argument, 'index': indexFile}


def indexGenome(referenceGenome: str) -> dict[str, str]:
    indexFile = ''.join([referenceGenome, ".fai"])

    argument = "samtools faidx {0}".format(referenceGenome)
    os.system(argument)

    return {'command': argument, 'index': indexFile}


def depthBam(bam: str, defaultFilesPath: dict[str, str], out: str) -> dict[str, str]:
    outputTxt = os.path.join(out, defaultFilesPath['depth'])
    
    argument = "samtools depth -a {0} > {1}".format(bam, outputTxt)
    os.system(argument)

    return {'command': argument, 'depth': outputTxt}
