import os
import re
import json

from core.raises.custom import QuantityMismatch, DataNotFound
from core.validator.types import vector


def formatKnownSites(sitesList: vector) -> str:
    numSites = len(sitesList)

    if numSites == 0:
        raise DataNotFound('Sites list given to formatter has lenght 0 or is NULL. Check knownSitesDirParser function result as it is the resposible to parse the vcf files.')

    sitesCommand = '--known-sites' + ' ' + sitesList[0]

    if numSites > 1:
        for site in sitesList[1:]:
            newSite = '--known-sites' + ' ' + site
            sitesCommand += ' ' + newSite

    return sitesCommand


def knownSitesDirParser(sitesDir: str) -> dict[str, list[str]]:
    vcfFiles = [os.path.join(sitesDir, f) for f in os.listdir(sitesDir) if re.search('\.vcf(?:\.gz)?$', f)]
    tbiFiles = [os.path.join(sitesDir, f) for f in os.listdir(sitesDir) if re.search('\.vcf(?:\.gz)?\.tbi$', f)]

    numVcfFiles = len(vcfFiles)
    numTbiFiles = len(tbiFiles)

    if numVcfFiles == 0 or numTbiFiles == 0:
        raise DataNotFound('No Files found. Known sites are vcf or vcf.gz and indexes are vcf.tbi or vcf.gz.tbi')
    elif numVcfFiles != numTbiFiles:
        raise QuantityMismatch('Gatk expects all of its known sites to have their indexes in the same directory. Sites found =',
                               numVcfFiles, 'indexes found =', numTbiFiles)

    return {'sites': vcfFiles, 'indexes': tbiFiles}


def fastqDirParser(fastqDir: str) -> dict[str, str]:
    fastqFiles = [f for f in os.listdir(fastqDir) if re.search('\.f(?:ast)?q(?:\.gz)?$', f)]
    numFiles = len(fastqFiles)

    if numFiles == 0:
        raise DataNotFound('No fastq, fq or compressed data in gz with those extensions found in directory', fastqDir)
    elif numFiles >= 3:
        raise QuantityMismatch('Expected 1 or 2 fastq files in directory, but found', numFiles)

    if numFiles == 1:
        fastq1 = os.path.join(fastqDir, fastqFiles[0])
        fastq2 = ''

        return {'trimmingMode': 'SE', 'file1': fastq1, 'file2': fastq2}

    # check which files are forward and backward
    if re.search('_1\.', fastqFiles[0]):
        fastq1 = os.path.join(fastqDir, fastqFiles[0])
        fastq2 = os.path.join(fastqDir, fastqFiles[1])
    else:
        fastq1 = os.path.join(fastqDir, fastqFiles[1])
        fastq2 = os.path.join(fastqDir, fastqFiles[0])

    return {'trimmingMode': 'PE', 'file1': fastq1, 'file2': fastq2}


def fastaDirParser(fastaDir: str) -> str:
    fastaFiles = [os.path.join(fastaDir, f) for f in os.listdir(fastaDir) if re.search('\.fa(?:sta)?$', f)]
    
    numFastaFiles = len(fastaFiles)

    if numFastaFiles == 0:
        raise DataNotFound('No fasta or fa files found in directory', fastaDir)

    return fastaFiles[0]


def extractFileName(filePath: str) -> str:
    delimiters = "_", ".", "#"
    base_name = os.path.splitext(os.path.basename(filePath))[0] # type: ignore
    regex_pattern = '|'.join(map(re.escape, delimiters))
    splitted = re.split(regex_pattern, base_name)

    return splitted[0]


def absolutePath(fileName: str, baseDir: str = os.getcwd()) -> str:
    filePath = os.path.join(baseDir, fileName)
    
    return os.path.normpath(filePath)


def loadJson(filepath: str) -> dict:
    try:
        with open(filepath) as f:
            return json.load(f)
    except Exception as e:
        raise Exception(str(e))
