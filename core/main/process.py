from core.main.pretify import printLine
from core.tools.trimmomatic import trimmingPE, trimmingSE
from core.tools.bwa import alignmentPE, alignmentSE, createRgLine, indexBWA
from core.tools.samtools import indexGenome
from core.tools.gatk import dictGenome, markDuplicatesSpark, baseRecalibrator, applyBQSR, variantIdentification, alignmentSummaryMetrics, insertSizeMetrics, selectVariants
from core.parser.path import loadJson, absolutePath, fastqDirParser, fastaDirParser, knownSitesDirParser, formatKnownSites, extractFileName
from core.validator.exists import extensionsExist


def appProcess(arguments: dict) -> None:
    try:
        out = arguments['path']
        trimming = arguments['useTrimming']

        defaultParameters = loadJson(absolutePath('defaults/parameters.json'))
        defaultFilesPath = loadJson(absolutePath('defaults/files.json'))

        fastqPaths = fastqDirParser(out)
        referenceGenome = fastaDirParser(out)
        sitesPaths = knownSitesDirParser(out)
        sitesFormated = formatKnownSites(sitesPaths['sites'])

        trimmomaticPaths = defaultFilesPath['trimmomatic']
        bwaPaths = defaultFilesPath['bwa']
        gatkPath = defaultFilesPath['gatk']
        adapterPaths = defaultFilesPath['adapter']

        file1 = fastqPaths['file1']
        file2 = fastqPaths['file2']
        sequenceMode = fastqPaths['trimmingMode']

        adapterChosen = defaultParameters['trimmomatic']['adapter']['default']
        encoding = defaultParameters['trimmomatic']['encoding']['default']
        leading = defaultParameters['trimmomatic']['leading']['default']
        trailing = defaultParameters['trimmomatic']['trailing']['default']
        slidingWindowSize = defaultParameters['trimmomatic']['slidingWindowSize']['default']
        slidingWindowMinQuality = defaultParameters['trimmomatic']['slidingWindowMinQuality']['default']
        seedMismatches = defaultParameters['trimmomatic']['seedMismatches']['default']
        palindromeClipThreshold = defaultParameters['trimmomatic']['palindromeClipThreshold']['default']
        simpleClipThreshold = defaultParameters['trimmomatic']['simpleClipThreshold']['default']
        minlen = defaultParameters['trimmomatic']['minlen']['default']
        threads = defaultParameters['shared']['threads']['default'] 

        adapter = absolutePath(adapterPaths[sequenceMode][adapterChosen])
        sm = extractFileName(file1)
        pl = defaultParameters['bwa']['pl']['default'] 
        pm = defaultParameters['bwa']['pm']['default'] 
        lb = defaultParameters['bwa']['lb']['default'] 
        sid = f"{sm}_{lb}"
        rgLine = createRgLine(sid, lb, pl, pm, sm)

        # create index with bwa, samtools and gatk; when they are not pre indexed
        if not extensionsExist(referenceGenome, ['.amb', '.ann', '.bwt', '.pac', '.sa']):
            _ = indexBWA(referenceGenome)
        
        if not extensionsExist(referenceGenome, ['.fai']):
            _ = indexGenome(referenceGenome)
        
        if not extensionsExist(referenceGenome, ['.dict'], True):
            _ = dictGenome(referenceGenome)

        # if user wants trimming, then use trimmomatic and asign new file1 and file2
        if trimming and sequenceMode == 'SE':
            trimmingResult = trimmingSE(threads, encoding, file1, adapter, leading, trailing, slidingWindowSize, slidingWindowMinQuality, seedMismatches, palindromeClipThreshold, simpleClipThreshold, minlen, trimmomaticPaths, out)
            file1 = trimmingResult['trimmed1']

        elif trimming and sequenceMode == 'PE':
            trimmingResult = trimmingPE(threads, encoding, file1, file2, adapter, leading, trailing, slidingWindowSize, slidingWindowMinQuality, seedMismatches, palindromeClipThreshold, simpleClipThreshold, minlen, trimmomaticPaths, out)   
            file1 = trimmingResult['trimmed1']
            file2 = trimmingResult['trimmed2'] 
        
        # alignment over referenceGenome and fastq files
        if sequenceMode == 'SE':
            alignmentResult = alignmentSE(referenceGenome, rgLine, threads, file1, bwaPaths, out)
        else:
            alignmentResult = alignmentPE(referenceGenome, rgLine, threads, file1, file2, bwaPaths, out)
            
        sam = alignmentResult['sam']

        # mark duplicates from sam file which is sorted and transform to bam by this method
        markDuplicatesResult = markDuplicatesSpark(sam, gatkPath, out)
        markedBam = markDuplicatesResult['marked']

        # create metrics files
        _ = alignmentSummaryMetrics(markedBam, referenceGenome, gatkPath, out)
        _ = insertSizeMetrics(markedBam, referenceGenome, gatkPath, out)

        # recalibrate marked result
        baseRecalibratorResult = baseRecalibrator(markedBam, referenceGenome, sitesFormated, gatkPath, out)
        recalTable = baseRecalibratorResult['recalTable']

        # aplly bsqr
        applyBQSRResult = applyBQSR(markedBam, recalTable, gatkPath, out)
        recalibratedBam = applyBQSRResult['recalibrated']

        # identify variants
        variantIdentificationResult = variantIdentification(referenceGenome, recalibratedBam, gatkPath, out)
        rawVariants = variantIdentificationResult['variants']

        # get indels and snps
        selectVariantsResult = selectVariants(rawVariants, referenceGenome, gatkPath, out)
        snps = selectVariantsResult['snps']
        indels = selectVariantsResult['indels']
        
    except KeyboardInterrupt:
        printLine('Keyboard interruption. Process stopped.')
    except Exception as error:
        printLine(str(error))
