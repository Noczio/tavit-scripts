import shutil
from typing import Callable


def separator(char: str = '═') -> None:
    width = shutil.get_terminal_size().columns
    separator = char * width
    print(f"\n{separator}\n")


def separatorBefore(func: Callable):
    def wrapper(*args, **kwargs):
        separator()
        result = func(*args, **kwargs)
        return result
    return wrapper


@separatorBefore
def printLine(*args) -> None:
    message = ' '.join(args)
    print(message)
