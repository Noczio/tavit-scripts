# use an official Conda runtime as a parent image
FROM continuumio/miniconda3:latest

# system libraries
RUN apt-get update && \
apt-get upgrade -y && \
apt-get clean all && \
apt-get purge && \
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# create app container
RUN mkdir /app

# copy the current directory contents into the container at /app
COPY . /app

# Set the working directory to /app
WORKDIR /app

# create a Conda environment with the required packages
RUN conda env create -f environment.yml
RUN conda init bash
RUN echo "conda activate tavit-scripts" >> ~/.bashrc

# path to env "opt/conda/envs/tavit-scripts/bin/R"
ENV PATH /opt/conda/envs/tavit-scripts/bin:$PATH

ENTRYPOINT ["bash"]