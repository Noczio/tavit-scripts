from core.main.pretify import printLine
from core.main.process import appProcess
from core.request.question import askForPath


def main() -> None:
    try:
        arguments = askForPath('Invalid path. Not such folder exists.', 'Usage: python app.py /path/to/folder', 'Trimming argument is incorrect. Usage: python app.py /path/to/folder <-trim, --t>')
        appProcess(arguments)
    except Exception as error:
        printLine(str(error))


if __name__ == "__main__":
    main()
