# Description

This repository is a Python pipeline with Trimmomatic, BWA, Samtools and Gatk4. Please, support and cite the tools used by Tavit, as it would be impossible to be created without them.

* Trimmomatic: <https://github.com/usadellab/Trimmomatic>

* BWA: <https://github.com/lh3/bwa>

* Samtools: <https://github.com/samtools/samtools>

* Gatk4: <https://github.com/broadinstitute/gatk>

## Launch app from pre-compiled image

Docker must be installed on the host system.

1. Download image from docker hub.

> docker pull devnocz/tavit-scripts:latest

2. Run the image.

* your/path/to/folder needs to be changed to the path of your desired volume.

> docker run -it --rm -v /your/path/to/folder:/external devnocz/tavit-scripts:latest

3. Execute app. Here in this example /your/path/to/folder should be /external or other subdirectory inside

> python app.py /your/path/to/folder

## Launch app from source code

Anaconda/Miniconda must be installed on the host system.

1. Download code as a zip and unzip it.

2. Navigate to where it was downloaded and open a terminal.

3. Create conda env from yml file:

> conda env create -f environment.yml

4. Activate conda env:

> conda activate tavit-scripts

5. Run app with Python:

> python app.py /your/path/to/folder

# Notes

1. This app expects fastq sequences from illumina HiSeq machines, a fasta reference genome and the known sites with its indexes.

2. Paramaters like threads, milen and encoding can be changed from defaults/parameters.json

* Go to the default key of the parameter and change it to desired value. Those that have a minium and max value are listed next to its default. null means it has no limit.

* Do not change "pl", "pm" and "lb" inside bwa or "adapter" in trimmomatic, those are hardcoded into the app.

3. Trimming is disabled by default. If needed, add -trim or --t after the path. Usage:

> python app.py /your/path/to/folder -trim

> python app.py /your/path/to/folder --t

4. If you provide in the directory folder the index files created by BWA mem, samtools faidx or gatk CreateSequenceDictionary on that particular referefence genome, then that step will be skiped. Samtools indexing and gatk creation of dictionary don't take any significant time, but BWA mem does (40'ish minutes on a i7 11700kf CPU).

5. If you're in need of the known sites and the reference genome for homosapiens it can be found here: <https://drive.google.com/drive/folders/1gGUJGepv7P1WEH9Sfvu4-lAjbtZqT24g?usp=share_link>. Sequences in fastq, fq or fastq.gz, fq.gz for testing can be downloaded from the NCBI website with sra-tools.
